const userCtrl = {};
const mysqlConnection = require('../database.js');
const bcrypt = require('bcryptjs');

//Registro de usuario
userCtrl.postUser = async (req, res) => {
    const {nombre, password, email} = req.body;    
    mysqlConnection.query('INSERT INTO usuarios(nombre, password, email) VALUES(?,?,?)', [nombre, await bcrypt.hash(req.body.password, 4), email], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true 
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//Login de usuario
userCtrl.getUser = async (req, res) => {
    mysqlConnection.query("SELECT * FROM usuarios WHERE email = ? ", [req.params.email], function(error, results, fields){
        try{
            if (results[0].password){
                bcrypt.compare(req.params.password, results[0].password, function(err, result){
                    if(result){
                        res.json({
                            ok: true,
                            resp: results[0]
                        });
                    }else{
                        res.json({ 
                            ok: false,
                            error: "Password INCORRECTA"
                        });
                    }
                });
            }
        }catch(error){
            res.json({ 
                ok: false,
                error: "Email NO VALIDO"
            });
        }
    });
}

//Eliminar un usuario
userCtrl.deleteUser = async (req, res) => {
    mysqlConnection.query('DELETE FROM usuarios WHERE id = ?', [req.params.id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                info:'Usuario ELIMINADO'
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//Modificación de usuario
userCtrl.editUser = async (req, res) => {
    const {nombre, password, email} = req.body; 
    mysqlConnection.query('UPDATE usuarios SET nombre=?, password=?, email=? WHERE id = ?', [nombre, await bcrypt.hash(password, 4), email, req.params.id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                info:'Usuario MODIFICADO'
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

module.exports = userCtrl;