const adopcionCtrl = {}
const mysqlConnection = require('../database.js');


//get todas las adopciones 
adopcionCtrl.getAdopciones = async (req, res) => {
    mysqlConnection.query('SELECT a.id, a.fecha, m.name, m.nacimiento, m.url_imagen, p.apellido, p.nombre, o.nombre AS ong FROM adopciones '+ 
                        'AS a LEFT JOIN mascotas AS m ON (a.id_mascota=m.id) INNER JOIN adoptantes AS p ON (a.id_adoptante=p.id)'+
                        ' INNER JOIN ongs AS o ON (a.id_ong=o.id)', (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows,
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adopciones por id 
adopcionCtrl.getAdopcionId = async(req, res) =>{
    const { id } = req.params;
    mysqlConnection.query('SELECT * FROM adopciones WHERE id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adopciones por id_ong
adopcionCtrl.getAdopcionOng = async(req, res) =>{
    mysqlConnection.query('SELECT a.id, a.fecha, m.name, m.nacimiento, m.url_imagen, p.apellido, p.nombre, o.nombre AS ong FROM adopciones '+ 
                        'AS a LEFT JOIN mascotas AS m ON (a.id_mascota=m.id) INNER JOIN adoptantes AS p ON (a.id_adoptante=p.id)'+
                        ' INNER JOIN ongs AS o ON (a.id_ong=o.id) WHERE a.id_ong = ?', [req.params.id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adopciones por id_mascota
adopcionCtrl.getAdopcionMascota = async(req, res) =>{
    mysqlConnection.query('SELECT a.id, a.fecha, m.name, m.nacimiento, m.url_imagen, p.apellido, p.nombre, o.nombre AS ong FROM adopciones '+ 
                    'AS a LEFT JOIN mascotas AS m ON (a.id_mascota=m.id) INNER JOIN adoptantes AS p ON (a.id_adoptante=p.id)'+
                    ' INNER JOIN ongs AS o ON (a.id_ong=o.id) WHERE a.id_mascota = ?', [req.params.id_mascota], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adopciones por id_adoptante
adopcionCtrl.getAdopcionAdoptante = async(req, res) =>{
    mysqlConnection.query('SELECT a.id, a.fecha, m.name, m.nacimiento, m.url_imagen, p.apellido, p.nombre, o.nombre AS ong FROM adopciones '+ 
                        'AS a LEFT JOIN mascotas AS m ON (a.id_mascota=m.id) INNER JOIN adoptantes AS p ON (a.id_adoptante=p.id)'+
                        ' INNER JOIN ongs AS o ON (a.id_ong=o.id) WHERE a.id_adoptante = ?', [req.params.id_adoptante], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adopciones por fecha
adopcionCtrl.getAdopcionFecha = async(req, res) =>{
    mysqlConnection.query('SELECT a.id, a.fecha, m.name, m.nacimiento, m.url_imagen, p.apellido, p.nombre, o.nombre AS ong FROM adopciones '+ 
                        'AS a LEFT JOIN mascotas AS m ON (a.id_mascota=m.id) INNER JOIN adoptantes AS p ON (a.id_adoptante=p.id)'+
                        ' INNER JOIN ongs AS o ON (a.id_ong=o.id) WHERE a.fecha > ?', [req.params.fec], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adopciones por fecha y ong
adopcionCtrl.getAdopcionFechaONG = async(req, res) =>{
    mysqlConnection.query('SELECT a.id, a.fecha, m.name, m.nacimiento, m.url_imagen, p.apellido, p.nombre, o.nombre AS ong FROM adopciones '+ 
                        'AS a LEFT JOIN mascotas AS m ON (a.id_mascota=m.id) INNER JOIN adoptantes AS p ON (a.id_adoptante=p.id)'+
                        ' INNER JOIN ongs AS o ON (a.id_ong=o.id) WHERE a.fecha > ? AND a.id_ong = ?', [req.params.fec, req.params.id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

module.exports = adopcionCtrl;

