const mascotaCtrl = {}
const mysqlConnection = require('../database.js');

//nueva mascota
mascotaCtrl.createMascota = async(req, res) => {
    const { name, raza, edad, tamanio, peso, sexo, nacimiento, url_imagen, descripcion, color, id_ong } = req.body;

    let sql = 'INSERT INTO mascotas(name, raza, edad, tamanio, peso, sexo, nacimiento, url_imagen, descripcion, color, id_ong) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
    var valores = [name, raza, edad, tamanio, peso, sexo, nacimiento, url_imagen, descripcion, color, id_ong];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true 
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
};


// GET all mascotas
mascotaCtrl.getMascotas = async(req, res) =>{

    mysqlConnection.query('SELECT m.id, m.name, m.raza, m.tamanio, m.peso, m.sexo, m.nacimiento, m.url_imagen, m.descripcion, m.color, m.id_ong, o.nombre AS ONG ' +
                        'FROM puppitdb.mascotas AS m LEFT JOIN puppitdb.ongs AS o ON (m.id_ong=o.id) WHERE m.estado_adopcion = 0', (err, rows, fields) => {

        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
        
    });

}


// get mascota en adopcion por ong_id
mascotaCtrl.getMascotaEnAdopcionONG = async(req, res) =>{
    const { id_ong } = req.params;
    mysqlConnection.query('SELECT * FROM mascotas WHERE id_ong = ? AND estado_adopcion = 0', [id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}


// GET An mascotas por Id de organizacion
mascotaCtrl.getMascotaXong = async(req, res) =>{
    const { id_ong } = req.params;
    mysqlConnection.query('SELECT m.id, m.name, m.raza, m.tamanio, m.peso, m.sexo, m.nacimiento, m.url_imagen, m.descripcion, m.color, m.id_ong, o.nombre AS ONG ' +
                        'FROM puppitdb.mascotas AS m LEFT JOIN puppitdb.ongs AS o ON (m.id_ong=o.id) WHERE m.id_ong = ?', [id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

// GET contar mascotas adoptadas por Id de organizacion
mascotaCtrl.getCantMascotaXong = async(req, res) =>{
    const { id_ong } = req.params;
    mysqlConnection.query('SELECT count(*) as cantidad ' +
                        'FROM puppitdb.mascotas AS m LEFT JOIN puppitdb.ongs AS o ON (m.id_ong=o.id) WHERE m.id_ong = ? and m.estado_adopcion = 1', [id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

// GET An mascota por provincia , tamanio
mascotaCtrl.getMascotaXProvTamEdad = async(req, res) =>{
    const { provincia } = req.params;
    const { tamanio } = req.params;
    mysqlConnection.query('SELECT m.id, m.name, m.raza, m.tamanio, m.peso, m.sexo, m.nacimiento, TIMESTAMPDIFF(Year,nacimiento,CURDATE()) AS edad_actual, TIMESTAMPDIFF(Month,nacimiento,CURDATE()) AS edad_actual_mes, m.url_imagen, m.descripcion, m.color, m.id_ong, o.nombre AS ONG, o.provincia FROM puppitdb.mascotas AS m LEFT JOIN puppitdb.ongs AS o ON (m.id_ong=o.id) WHERE o.provincia = ? AND m.tamanio = ? AND m.estado_adopcion = 0'
                        , [provincia, tamanio], (err, rows, fields) => {
                             
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

// GET An mascota por Id
mascotaCtrl.getMascota = async(req, res) =>{
    const { id } = req.params;
    mysqlConnection.query('SELECT id, name, raza, tamanio, peso, sexo, nacimiento, TIMESTAMPDIFF(Year,nacimiento,CURDATE()) AS edad_actual, TIMESTAMPDIFF(Month,nacimiento,CURDATE()) AS edad_actual_mes, url_imagen, descripcion, color, id_ong FROM mascotas WHERE id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
} 

// GET mascotas adoptadas de una ONG
mascotaCtrl.getMascotasAdoptadasxONG = async(req, res) =>{
    mysqlConnection.query('SELECT * FROM mascotas WHERE estado_adopcion=1 AND id_ong=?',[req.params.id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//Modificar mascota
mascotaCtrl.editMascota = async (req, res) => {
    const { id } = req.params;
    const { name, raza, edad, tamanio, peso, sexo, nacimiento, url_imagen, descripcion, color, id_ong, estado_adopcion } = req.body;
    mysqlConnection.query('UPDATE mascotas SET name=?, raza=?, edad=?, tamanio=?, peso=?, sexo=?, nacimiento=?, url_imagen=?, descripcion=?, color=?, id_ong=?, estado_adopcion=? WHERE id = ?', [name, raza, edad, tamanio, peso, sexo, nacimiento, url_imagen, descripcion, color, id_ong, estado_adopcion,id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                info:'mascota modificada'
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//obtener solo mascotas en adopcion 
mascotaCtrl.getMascotasEnAdopcion = async(req, res) =>{
    const { estado_adopcion } = req.params;
    mysqlConnection.query('SELECT * FROM mascotas WHERE estado_adopcion = ?', [estado_adopcion], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}


module.exports = mascotaCtrl;