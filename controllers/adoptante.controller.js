const adoptanteCtrl = {}
const mysqlConnection = require('../database.js');


// get todas las adoptantes
adoptanteCtrl.getAdoptantes = async (req, res) => {
    mysqlConnection.query('SELECT * FROM adoptantes', (err, rows, fields) => {

        if (!err) {
            res.json({ 
                ok: true,
                rows: rows,
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
        
    });
}

//new Adoptante
adoptanteCtrl.createAdoptante = async(req, res) => {
    const { apellido, nombre, dni, codigo_postal, direccion, email, celular, fechaNacimiento, sexo } = req.body;

    let sql = 'INSERT INTO adoptantes (apellido, nombre, dni, codigo_postal, direccion, email, celular, fechaNacimiento, sexo) VALUES (?,?,?,?,?,?,?,?,?)';
    var valores = [apellido, nombre, dni, codigo_postal, direccion, email, celular, fechaNacimiento, sexo];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true 
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
};

//get adoptante por id
adoptanteCtrl.getAdoptante = async (req, res) => {
    mysqlConnection.query('SELECT * FROM adoptantes WHERE id = ?',[req.params.id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows,
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//get adoptantes por ONG
adoptanteCtrl.getAdoptantesONG = async (req, res) => {
    mysqlConnection.query('SELECT a.id, a.apellido, a.nombre FROM adoptantes AS a LEFT JOIN adopciones AS b ON (a.id=b.id_adoptante) WHERE b.id_ong = ?',[req.params.id_ong], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows,
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

module.exports = adoptanteCtrl;