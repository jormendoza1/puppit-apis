const ongCtrl = {}
const mysqlConnection = require('../database.js');

//nueva ONG
ongCtrl.createONG = async(req, res) => {
    const { celular, nombre, direccion, codigo_postal, email, link_donacion, terminos_adopcion, id_usuario } = req.body;

    let sql = 'INSERT INTO ongs(celular, nombre, direccion, codigo_postal, email, link_donacion, terminos_adopcion, id_usuario) VALUES (?,?,?,?,?,?,?,?)';
    var valores = [celular, nombre, direccion, codigo_postal, email, link_donacion, terminos_adopcion, id_usuario];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true 
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
};


//GET todas ONG's
ongCtrl.getONGS = async(req, res) =>{
    mysqlConnection.query('SELECT * FROM ongs', (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//Modificación de ONG
ongCtrl.editONG = async (req, res) => {
    const { id } = req.params;
    const {celular, nombre, direccion, codigo_postal, email, link_donacion, terminos_adopcion, provincia} = req.body; 
    mysqlConnection.query('UPDATE ongs SET celular=?, nombre=?, direccion=?, codigo_postal=?, email=?, link_donacion=?, terminos_adopcion=?, provincia=? WHERE id_usuario = ?', [celular, nombre, direccion, codigo_postal, email, link_donacion, terminos_adopcion, provincia, id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                info:'ONG modificada'
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//GET ONG por id
ongCtrl.getONG = async(req, res) =>{
    const { id } = req.params;
    mysqlConnection.query('SELECT * FROM ongs WHERE id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//GET todas provincias de ONG's
ongCtrl.getProvONGS = async(req, res) =>{
    mysqlConnection.query('SELECT DISTINCT provincia FROM ongs', (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

//GET ONG según userId
ongCtrl.getONGByUserId = async(req, res) =>{
    mysqlConnection.query('SELECT * FROM ongs WHERE id_usuario = ?',[req.params.id_usuario], (err, rows, fields) => {
        if (!err) {
            res.json({ 
                ok: true,
                rows: rows
            });
        } else {
            res.json({ 
                ok: false,
                error: err
            });
        }
    });
}

module.exports = ongCtrl;