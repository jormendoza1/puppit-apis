const ongCtrl = require('./../controllers/ong.controller');

//Manejador de rutas
const express = require('express');
const router = express.Router();

//Rutas para la gestion de ONG's
router.post('/', ongCtrl.createONG);
router.get('/', ongCtrl.getONGS);
router.put('/:id', ongCtrl.editONG);
router.get('/user/:id_usuario', ongCtrl.getONGByUserId);
router.get('/provincia', ongCtrl.getProvONGS);
router.get('/:id', ongCtrl.getONG);

module.exports = router;