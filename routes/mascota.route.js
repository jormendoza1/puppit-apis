const mascotaCtrl = require('./../controllers/mascota.controller');

//Manejador de rutas
const express = require('express');
const router = express.Router();

//Rutas para la gestion de mascota
router.post('/', mascotaCtrl.createMascota);
router.get('/', mascotaCtrl.getMascotas);
router.get('/ong/:id_ong', mascotaCtrl.getMascotaXong);
router.get('/:id', mascotaCtrl.getMascota);
router.get('/mascotas/adoptadas/:id_ong', mascotaCtrl.getMascotasAdoptadasxONG);
router.get('/estado/:estado_adopcion', mascotaCtrl.getMascotasEnAdopcion);
router.put('/modificar/:id', mascotaCtrl.editMascota);
router.get('/ong/adopciones/:id_ong', mascotaCtrl.getMascotaEnAdopcionONG);
router.get('/ong/contar/:id_ong', mascotaCtrl.getCantMascotaXong);
router.get('/filtro/:provincia/:tamanio', mascotaCtrl.getMascotaXProvTamEdad);

module.exports = router;