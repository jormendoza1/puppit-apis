const adopcionCtrl = require('./../controllers/adopcion.controller');

//Manejador de rutas
const express = require('express');
const router = express.Router();

//Rutas para la gestion de adopciones
//router.post('/', adopcionCtrl.createAdopcion);
router.get('/', adopcionCtrl.getAdopciones);
router.get('/ong/:id_ong', adopcionCtrl.getAdopcionOng);
router.get('/:id', adopcionCtrl.getAdopcionId);
router.get('/filtro/mascota/:id_mascota', adopcionCtrl.getAdopcionMascota);
router.get('/filtro/adoptante/:id_adoptante', adopcionCtrl.getAdopcionAdoptante);
router.get('/filtro/fecha/:fec', adopcionCtrl.getAdopcionFecha);
router.get('/filtro/fecha/:fec/:id_ong', adopcionCtrl.getAdopcionFechaONG);


module.exports = router;