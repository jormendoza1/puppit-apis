const userCtrl = require('./../controllers/usuario.controller'); 

//Manejador de rutas 
const express = require('express'); 
const router = express.Router(); 

//Rutas para gestion de usuario 
router.post('/', userCtrl.postUser);
router.get('/:email/:password', userCtrl.getUser);
router.delete('/:id', userCtrl.deleteUser);
router.put('/:id', userCtrl.editUser);
 
module.exports = router;