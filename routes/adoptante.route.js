const adoptanteCtrl = require('./../controllers/adoptante.controller');

//Manejador de rutas
const express = require('express');
const router = express.Router();

//Rutas para la gestion de adoptantes
router.post('/', adoptanteCtrl.createAdoptante);
router.get('/', adoptanteCtrl.getAdoptantes);
router.get('/:id', adoptanteCtrl.getAdoptante);
router.get('/adopcion-ong/:id_ong', adoptanteCtrl.getAdoptantesONG);
//router.get('/ong/:id_ong', adopcionCtrl.getAdopcionOng);
//router.get('/:id', adopcionCtrl.getAdopcionId);

module.exports = router;